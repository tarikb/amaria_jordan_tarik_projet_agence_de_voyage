<?php

require_once 'ConfigModel.php';

    class HomeModel {

        static function listVoyageDetente() {  //fonction qui permet d'afficher la liste des voyages
            $bdd = connect();
            $reponse = $bdd->query('SELECT * FROM voyage WHERE type = 2');
            $voyage = $reponse->fetchAll();
            return $voyage;
        }

        static function listVoyageXtrem() {  //fonction qui permet d'afficher la liste des voyages
            $bdd = connect();
            $reponse = $bdd->query('SELECT * FROM voyage WHERE type = 1');
            $voyage = $reponse->fetchAll();
            return $voyage;
        }
    }

?>