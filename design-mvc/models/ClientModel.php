<?php

include_once 'ConfigModel.php';

    class Client {
        //      Permet de s'inscrire
        static function register($login, $password, $mail, $nom, $prenom){
            $bdd = self::connection();
            $request = $bdd->prepare('INSERT INTO client VALUES(NULL, :login, :password, :mail, :nom, :prenom)');
            $request->execute(['login' => $login, 'password' => $password, 'mail' => $mail, 'nom' => $nom, 'prenom' => $prenom]);
        }

        //      Permet de se connecter
        static function login($login, $password){
            $bdd = self::connection();
            $request = $bdd->prepare('SELECT id, login FROM client WHERE login = :login AND password = :password');
            $request->execute(['login' => $login, 'password' => $password]);
            $fetch = $request->fetchAll(); 
            return $fetch;
        }
    }
?>