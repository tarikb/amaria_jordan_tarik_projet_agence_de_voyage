<?php 
 
require_once 'controllers/Controller.php'; 
require_once(__DIR__.'/../models/InscriptionModel.php'); 
 
 
class InscriptionController extends Controller { 
 
    function __construct () { 
 
 
    } 
 
    public function getInscription() { 
        echo $this->getTwig()->render('InscriptionView.twig'); 
    } 
 
 
    public function postInscription() { 
        $prenom=$_POST['prenom']; 
        $nom=$_POST['nom']; 
        $mail = $_POST['mail']; 
        $login = $_POST['login']; 
        $password=$_POST['password']; 
        $pass_hash = hash("sha256",$password); 
         
        $inscription = InscriptionModel::inscription($login,$mail,$pass_hash,$nom,$prenom); 
        // $data = ["array1" => $inscription]; 
 
        if ($inscription) { 
            // regiriger vers la page d'accueil, ou afficher un message, ou afficher une autre vue 
            echo $this->getTwig()->render('ConnexionView.twig'); 
         
        } 
        else { 
            echo $this->getTwig()->render('InscriptionView.twig'); 
        } 
         
 
    } 
} 
 
 
 
?>