<?php


class UploadController extends Controller {

    public function getUpload(){

        echo $this->getTwig()->render('UploadView.twig');
    }

    public function postUpload(){
        
        $hasUpload = !empty($_FILES) && $_FILES['id']['size'] > 0;

        if ($hasUpload) {
            //             Rename du fichier
            $filename = $_FILES['id']['name'];
            $filename = preg_replace('/([^.a-z0-9]+)/i', '-', $filename);
            
            //             Défini le type de fichier à pouvoir être Upload
            $extensionsValides = ['pdf'];
            $pathinfo = pathinfo($_FILES['id']['name']);
            $extension = strtolower($pathinfo['extension']);

            if(in_array($extension, $extensionsValides)){
                //             Déplacer le fichier dans un dossier
                $destination = __DIR__."/../fichier/$filename";
                $resultat = move_uploaded_file($_FILES['id']['tmp_name'], $destination);
            }
            else {
                $resultat = false;
            }
        }
        else {
            $resultat = false;
        }

        echo $this->getTwig()->render('UploadView.twig', ['success' => $resultat]);
    }
}