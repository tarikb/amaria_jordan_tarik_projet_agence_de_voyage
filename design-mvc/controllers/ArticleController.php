<?php

require_once 'Controller.php';


class ArticleController extends Controller {

    function __construct () {
        require_once('models/ArticleModel.php');
    }

    public function getArticle () {
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $articleById = ArticleModel::getArticleById($id);
        $data = [
            'articles'=>$articleById,
        ];
        echo $this->getTwig()->render('ArticleView.twig',$data);
    }
}

?>