<?php

abstract class Controller {

   function getTwig() {
       require_once 'vendor/autoload.php';

       // On dit à TWIG où se trouvent nos templates
       $loader = new Twig_Loader_Filesystem('views/twig');

       // On créée notre objet twig
       return new Twig_Environment($loader);
   }
}

?>