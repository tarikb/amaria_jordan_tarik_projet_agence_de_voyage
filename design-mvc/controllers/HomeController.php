<?php

require_once 'controllers/Controller.php';

class HomeController extends Controller {

    function __construct () {
        require_once('models/HomeModel.php');

    }

    public function principal() {
        $arrayVoyageD = HomeModel::listVoyageDetente();
        $arrayVoyageX = HomeModel::listVoyageXtrem();
        $data = ["array1" => $arrayVoyageX, "array2" => $arrayVoyageD];
        echo $this->getTwig()->render('HomeView.twig',$data);

    }
}



?>