<?php

require_once 'controllers/Controller.php';
require_once 'models/PanierModel.php';

class PanierController extends Controller {

    function __construct() {
        
    }

    public function getPanier() {
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $PanierById = PanierModel::getPanierById($id);
        $data = [
            'article' => $PanierById,
        ];

        echo $this->getTwig()->render('PanierView.twig', $data);
    }
}