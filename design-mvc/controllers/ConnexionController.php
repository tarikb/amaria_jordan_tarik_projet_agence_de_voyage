<?php 
 
require_once 'controllers/Controller.php'; 
require_once(__DIR__.'/../models/InscriptionModel.php'); 
 
 
class ConnexionController extends Controller { 
 
    function __construct () { 
         
 
    } 
 
    public function getConnexion() { 
        // $login = $_POST['login']; 
        // $password=$_POST['password']; 
        // $pass_hash = hash("sha256",$password); 
         
         
        // InscriptionModel::connexion($login,$pass_hash); 
        //$data = ["array1" => $connexion]; 
        echo $this->getTwig()->render('ConnexionView.twig'); 
 
    } 
 
    public function postConnexion() { 
        $login = $_POST['login']; 
        $password=$_POST['password']; 
        $pass_hash = hash("sha256",$password); 
         
         
        InscriptionModel::connexion($login,$pass_hash); 
        //$data = ["array1" => $connexion]; 
        echo $this->getTwig()->render('ConnexionView.twig'); 
 
    } 
} 
 
 
 
?>