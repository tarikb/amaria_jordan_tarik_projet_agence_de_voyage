<?php
 
 
use Pecee\SimpleRouter\SimpleRouter;
include __DIR__.'/controllers/HomeController.php';
include __DIR__.'/controllers/ArticleController.php';
include __DIR__.'/controllers/ConnexionController.php';
include __DIR__.'/controllers/InscriptionController.php';
include __DIR__.'/controllers/PanierController.php';
include __DIR__.'/controllers/UploadController.php';
 
$prefix = '/amaria_jordan_tarik_projet_agence_de_voyage/design-mvc/';
 
SimpleRouter::group(['prefix' => $prefix], function () {
 
    // accueil
    SimpleRouter::get('/', 'HomeController@principal');
    SimpleROuter::get('/accueil', 'HomeController@principal');
 
    // page article
    SimpleRouter::get('/article', 'ArticleController@getArticle');
    SimpleRouter::post('/article', 'ArticleController@getArticle');
 
    // pages Inscription Connexion
    SimpleRouter::get('/inscription', 'InscriptionController@getInscription');
    SimpleRouter::post('/inscription', 'InscriptionController@postInscription');
 
    SimpleRouter::get('/connexion', 'ConnexionController@getConnexion');
    SimpleRouter::post('/connexion', 'ConnexionController@postConnexion');
 
    // page panier
    SimpleRouter::get('/panier', 'PanierController@getPanier');
    SimpleRouter::post('/panier', 'PanierController@getPanier');

     // page upload
    SimpleRouter::get('/upload', 'UploadController@getUpload');
    SimpleRouter::post('/upload', 'UploadController@postUpload');
 
});
 
?>