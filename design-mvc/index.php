<?php

/* Load external routes file */
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/routes.php';

use Pecee\SimpleRouter\SimpleRouter;

// Start the routing
SimpleRouter::start();